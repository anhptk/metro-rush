<div class="col-sm-10" id="right_side">
<h1 class="text-dark text-center mt-2">
Metro rush
</h1>
<div class="tab-content">
<div class="tab-pane active" id="core-project-09c283f9-5f18-4d25-91ba-3460c7fa038a">
<h4 class="text-center">
Core project
</h4>
<h6 class="text-center mb-5">
Notions: classes, objects, and graphs
</h6>
<table class="table-borderless table">
<tbody><tr>
<td>
<h3 class="mx-3 mb-4 border-bottom">Introduction</h3>
<div class="mx-3 text-justify readable_text">
<p>Here is the second project about graphs and object-oriented!</p><p>You get familiarized with graphs data structure and object-oriented in the last project. Perfect! Time to move the next level. Be ready, folks!</p>

</div>

</td>
</tr>
<tr>
<td>
<h3 class="mx-3 mb-4 border-bottom">Your mission</h3>
<div class="mx-3 text-justify readable_text">
<p>A <strong>transit map</strong> is a topological map of a public transport system. It is composed of lines, stations, and transfer points. A line connects several stations in a direct route from a first station to an end station. A transfer point is a station that is crossed by two or more lines. A transfer point allows passengers to change from a line to one other.</p><p>The current project requires you to find the smallest number of turns that is required for all the trains to move from a specified station (start point) to another specified station (end point).</p><p>There are a few constraints you have to respect:</p><ol>
<li>there can be only one train at a given station, except at the specified start and end points;</li>
<li>all the trains can move during a turn;</li>
<li>a train can move from a station to only one connected station of the current line during one turn;</li>
<li>a train can switch from a line to another at a transfer point during one turn;</li>
<li>a train can move from a station to another or it can switch from a line to another during one turn; it cannot do both during one turn.</li>
</ol>

</div>

</td>
</tr>
<tr>
<td>
<h3 class="mx-3 mb-4 border-bottom">Specifications</h3>
<div class="mx-3 text-justify readable_text">
<p>You will write a Python script, name&nbsp;<strong>metro_rush.py</strong>, which takes a filename as an argument. This file contains a list of metro lines and metro stations of the following form:</p><pre class="ql-syntax ql-indent-1">#&lt;line_name&gt;
1:&lt;station_name&gt;
2:&lt;station_name&gt;
3:&lt;station_name&gt;
(…)
#&lt;line_name&gt;
1:&lt;station_name&gt;
2:&lt;station_name&gt;
3:&lt;station_name&gt;
(…)

START=&lt;line_name&gt;:&lt;station_id&gt;
END=&lt;line_name&gt;:&lt;station_id&gt;
TRAINS=n
</pre><p>Where:</p><ul>
<li>where all stations in a line are connected in the order of their IDs. A station that has two or more lines passing through will appear with the same name in all lines.</li>
<li>a circular line will have the first and last ID with the same name.</li>
<li>all trains will get their own label Tx (where x has a unique value between 1 and n).</li>
<li>if the file is improperly formatted, your program will output "Invalid file" on <strong>stderr</strong> before exiting.</li>
</ul><p>To visualize the result, the program will print the state of the network at each turn in the following way: for each occupied station, print &lt;station_name&gt;(&lt;line_name&gt;:&lt;station_id&gt;)-&lt;train_label&gt;</p><p>Example:</p><pre class="ql-syntax">Subhash Nagar(Blue Line:17)-T9,T8,T7|Tagore Garden(Blue Line:18)-T6|Rajouri Garden(Blue Line:19)-T5|Ramesh Nagar(Blue Line:20)-T4|Moti Nagar(Blue Line:21)-T3,T2,T1
</pre><p>Note: Blue Line:17 &amp; Blue Line:21 can have more than one train because they are START and END stations, other stations can only have one train at a time.</p><p>The program stops when all trains have reached the end station.</p><p>Below is the picture of Delhi Metro network and example metro stations file that contain part of it. It's just for example, you can use this or other city metro map to create the metro stations file.</p><p>Source link: </p><p>https://delhimetrorail.info/delhi-metro-map</p><p>https://delhimetrorail.info/delhi-metro-stations</p>

</div>

</td>
</tr>
<tr>
<td>
<div class="mx-3 mb-4" data-turbolinks="false">
<i class="fas fa-file"></i>
<a href="https://intra.intek.io//uploads/material/file/file/24/delhi-metro-stations">delhi-metro-stations</a>

</div>

</td>
</tr>
<tr>
<td>
<div class="mx-3 my-5">
<div class="row">
<div class="offset-2 col-8">
<img src="https://intra.intek.io//uploads/material/image/image/12/delhimetro-map_eng.jpg" width="100%">
</div>
</div>
</div>


</td>
</tr>
<tr>
<td>
<h3 class="mx-3 mb-4 border-bottom">Evaluation</h3>
<div class="mx-3 text-justify readable_text">
<p>For the core part of the project, you're required moving at least 30 trains at a time from start station to end station without error or hanging your machine (maximum calculating time is 2 min)</p><p>There is no Sentinel for this project, as you are free in the choice of algorithms. Checking that you follow the constraint and the trains not skip any station in it path is trivial.</p><p>However, and as usual, you will have to demonstrate your understanding of the algorithms you implemented during the review.</p>

</div>

</td>
</tr>
</tbody></table>
<div class="d-flex px-5 pb-5">
<div class="mr-auto"></div>
<a class="text-dark fs-4" data-scroll="true" data-slug="bonus-multiple-algorithms-211" href="#">BONUS: Multiple algorithms
<i class="fas fa-arrow-right"></i>
</a></div>
</div>
<div class="tab-pane " id="bonus-multiple-algorithms-211">
<h4 class="text-center">
BONUS: Multiple algorithms
</h4>
<h6 class="text-center mb-5">
Notions: algorithm and inheritance
</h6>
<table class="table-borderless table">
<tbody><tr>
<td>
<h3 class="mx-3 mb-4 border-bottom">Multiple algorithms</h3>
<div class="mx-3 text-justify readable_text">
<p>For the bonus part, you will implement several algorithms calculating the smallest number of turns for the trains.&nbsp;As many as you want, each implementation will get you points... as long as you understand the algorithm!</p><p>You will modify your&nbsp;<code>metro_rush.py</code> program so that it accepts an option specifying the algorithm the program will run with to find the solution.</p>

</div>

</td>
</tr>
</tbody></table>
<div class="d-flex px-5 pb-5">
<a class="text-dark fs-4" data-scroll="true" data-slug="core-project-09c283f9-5f18-4d25-91ba-3460c7fa038a" href="#"><i class="fas fa-arrow-left"></i>
Core project
</a><div class="mr-auto"></div>
<a class="text-dark fs-4" data-scroll="true" data-slug="bonus-visualize-the-metro-network-with-pyglet" href="#">BONUS: Visualize the Metro Network  with Pyglet
<i class="fas fa-arrow-right"></i>
</a></div>
</div>
<div class="tab-pane " id="bonus-visualize-the-metro-network-with-pyglet">
<h4 class="text-center">
BONUS: Visualize the Metro Network  with Pyglet
</h4>
<h6 class="text-center mb-5">
Notions: visualize and pyglet
</h6>
<table class="table-borderless table">
<tbody><tr>
<td>
<h3 class="mx-3 mb-4 border-bottom">Visualize the Metro Network with Pyglet</h3>
<div class="mx-3 text-justify readable_text">
<p>Another big challenge for you: can you visualize the Metro Network as seen in the Tranport Map picture with Pyglet?</p><ul>
<li>The positions of the stations and the lines not necessary 100% like in the real world.</li>
<li>The trains and it movement between stations must be visualize too. (you should only use a color dot with the train label to represent a train)</li>
</ul>

</div>

</td>
</tr>
</tbody></table>
<div class="d-flex px-5 pb-5">
<a class="text-dark fs-4" data-scroll="true" data-slug="bonus-multiple-algorithms-211" href="#"><i class="fas fa-arrow-left"></i>
BONUS: Multiple algorithms
</a><div class="mr-auto"></div>
</div>
</div>
</div>
</div>